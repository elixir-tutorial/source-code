defmodule ApiServerWeb.Resolvers.Users do
  def list_users(_parent, _args, _resolution) do
    {:ok, ApiServer.Users.list_users()}
  end
end
