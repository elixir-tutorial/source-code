defmodule ApiServerWeb.Controllers.ApiController do
  use ApiServerWeb, :controller # will include the controller part from api_server_web.ex

  def index(conn, _params) do
    json(conn, %{message: "Hello World!"})
  end

end