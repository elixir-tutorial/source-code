defmodule ApiServerWeb.Schema do
  use Absinthe.Schema

  import_types(ApiServerWeb.Schema.UserTypes)

  alias ApiServerWeb.Resolvers

  query do
    @desc "Test endpoint"
    field :health, :string do
      resolve(fn _root, _args, _info -> {:ok, "working"} end)
    end

    @desc "All users"
    field :users, list_of(:user) do
      resolve(&Resolvers.Users.list_users/3)
    end
  end
end
