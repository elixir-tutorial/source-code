defmodule ApiServerWeb.Schema.UserTypes do
  use Absinthe.Schema.Notation

  object :user do
    @desc "User UUID"
    field(:id, :id)

    @desc "User email address"
    field(:email, :string)

    @desc "User first name"
    field(:first_name, :string)

    @desc "User last name"
    field(:last_name, :string)
  end
end
