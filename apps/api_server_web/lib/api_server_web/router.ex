defmodule ApiServerWeb.Router do
  use ApiServerWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api" do
    pipe_through(:api)

    get("/", ApiServerWeb.Controllers.ApiController, :index)

    forward("/graphql", Absinthe.Plug, schema: ApiServerWeb.Schema)
    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: ApiServerWeb.Schema)
  end
end
