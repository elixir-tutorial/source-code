# Since configuration is shared in umbrella projects, this file
# should only configure the :api_server_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :api_server_web,
  ecto_repos: [ApiServer.Repo],
  generators: [context_app: :api_server, binary_id: true]

# Configures the endpoint
config :api_server_web, ApiServerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bfvGQ7GBI9LwyJK5+cWPJiH94gGONbMNQmeRmf8NzLDo0Fnqr/5VV+OUbfWZPueG",
  render_errors: [view: ApiServerWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: ApiServerWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
