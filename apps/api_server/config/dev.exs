# Since configuration is shared in umbrella projects, this file
# should only configure the :api_server application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# Configure your database
config :api_server, ApiServer.Repo,
  username: "postgres",
  password: "postgres",
  database: "api_server_dev",
  hostname: "localhost",
  pool_size: 10
