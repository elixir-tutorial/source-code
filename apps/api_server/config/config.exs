# Since configuration is shared in umbrella projects, this file
# should only configure the :api_server application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

config :api_server, ecto_repos: [ApiServer.Repo]

import_config "#{Mix.env}.exs"
