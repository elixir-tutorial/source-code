# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     ApiServer.Repo.insert!(%ApiServer.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias ApiServer.User
alias ApiServer.Repo

%User{email: "filip@example.com", first_name: "Filip", last_name: "Vavera"} |> Repo.insert!
%User{email: "viktor@example.com", first_name: "Viktor", last_name: "Nawrath"} |> Repo.insert!