defmodule ApiServer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    Supervisor.start_link([
      ApiServer.Repo,
    ], strategy: :one_for_one, name: ApiServer.Supervisor)
  end
end
