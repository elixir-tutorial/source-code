defmodule ApiServer.Users do
  import Ecto.Query

  alias ApiServer.Repo
  alias ApiServer.User

  def list_users() do
    query = from(u in User)

    query
    |> Repo.all()
  end
end
